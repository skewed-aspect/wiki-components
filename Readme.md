# WikiJS Web Components

This is a series of web components for augmenting WikiJS and filling in missing features (or making up for it's lack 
of templating) by using custom [Web Components][]. This is a collection of several components, each with its own 
use case.

## Installation

...

## Components

### `<PageList>`
  * Arguments:
    * `path`: (`string`) Required. The path under which to list pages, with `/` being the root path.
    * `filter` : (`string`) Optional. An optional filter to exclude some pages.
    * `tags`: (`string`) Optional. A comma seperated list of tags the pages must match.

Displays a `<ul>` of page links based off of the path specified. For example, if I had four pages:

* `/foo/page1`
* `/foo/page2`
* `/foo/page2-extra`
* `/foo/page3`

Then I would use it like this:

```html
<page-list path="/foo" filter="page2"></page-list>
```

It should output the following:

```html
<ul>
    <li><a href="/foo/page1">Page 1 Title</a></li>
    <li><a href="/foo/page3">Page 3 Title</a></li>
</ul>
```

(This is because it skipped over the pages that matched the filter.)

[Web Components]: https://developer.mozilla.org/en-US/docs/Web/Web_Components
