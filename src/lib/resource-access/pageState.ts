//----------------------------------------------------------------------------------------------------------------------
// PageStateResourceAccess
//----------------------------------------------------------------------------------------------------------------------

class PageStateResourceAccess
{
    #store : Map<string, unknown> = new Map();

    set(key : string, value : any) : void
    {
        this.#store.set(key, value);
    }

    has(key : string) : boolean
    {
        return this.#store.has(key);
    }

    get<T>(key : string) : T | undefined
    {
        return this.#store.get(key) as T;
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new PageStateResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
