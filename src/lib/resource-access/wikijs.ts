//----------------------------------------------------------------------------------------------------------------------
// WikiJSResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import { WikiPageSummary } from '../../models/wikijs/pages';

//----------------------------------------------------------------------------------------------------------------------

interface PageListResponse
{
    data : {
        pages : {
            list : WikiPageSummary[];
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

class WikiJSResourceAccess
{
    async $runQuery<T>(query : string) : Promise<T>
    {
        const data = JSON.stringify({ query });

        const response = await fetch('/graphql', {
            method: 'post',
            body: data,
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length.toString(),
                'User-Agent': 'Node'
            }
        });

        return response.json();
    }

    async listPages() : Promise<WikiPageSummary[]>
    {
        const query = `{
            pages {
                list (orderBy: TITLE) {
                    id
                    path
                    title
                    tags
                }
            }
        }`;

        // Get back the pages
        const pages = await this.$runQuery<PageListResponse>(query);

        console.log('pages??', pages);

        // Return a sane list. (Screw GraphQL.)
        return (pages.data.pages.list as WikiPageSummary[]) ?? [];
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new WikiJSResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
