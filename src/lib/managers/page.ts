//----------------------------------------------------------------------------------------------------------------------
// PageManager
//----------------------------------------------------------------------------------------------------------------------

// Models
import { WikiPageSummary } from '../../models/wikijs/pages.ts';
import { NestedPages } from '../../models/pageList.ts';

// Engines
import pageEng from '../engines/page.ts';

// Resource Access
import pageStateRA from '../resource-access/pageState';
import wikijsRA from '../resource-access/wikijs';

//----------------------------------------------------------------------------------------------------------------------

class PageManager
{
    // We cache the pages since we only care about getting the list on page load. This prevents multiple calls per
    // PageList component per page.
    #pagesPromise ?: Promise<WikiPageSummary[]>;

    async loadPages() : Promise<void>
    {
        if(!this.#pagesPromise)
        {
            this.#pagesPromise = wikijsRA.listPages();
        }

        const pages = await this.#pagesPromise;
        pageStateRA.set('wiki:pages', pages);
    }

    async getPages(path : string, filter ?: string, tags : string[] = []) : Promise<NestedPages[]>
    {
        if(!pageStateRA.has('wiki:pages'))
        {
            await this.loadPages();
        }

        // Get the pages
        const pages = pageStateRA.get<WikiPageSummary[]>('wiki:pages') ?? [];

        // Filter the pages
        const filteredPages = pageEng.filterPages(pages, path, filter, tags);

        // Return the nested page object
        return pageEng.buildNestedPages(filteredPages, path);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new PageManager();

//----------------------------------------------------------------------------------------------------------------------
