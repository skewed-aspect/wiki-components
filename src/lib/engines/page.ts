//----------------------------------------------------------------------------------------------------------------------
// PageEngine
//----------------------------------------------------------------------------------------------------------------------

// Models
import { WikiPageSummary } from '../../models/wikijs/pages';
import { NestedPages } from '../../models/pageList.ts';

//----------------------------------------------------------------------------------------------------------------------

class PageEngine
{
    filterPages(pages : WikiPageSummary[], path : string, filter ?: string, tags : string[] = []) : WikiPageSummary[]
    {
        pages = pages
            .filter((page) =>
            {
                if(path[0] === '/')
                {
                    path = path.substring(1);
                }

                return page.path.startsWith(path);
            });

        if(filter)
        {
            const filterRE = new RegExp(filter);

            pages = pages
                .filter((page) =>
                {
                    return !page.path.match(filterRE);
                });
        }

        if(tags.length > 0)
        {
            pages = pages
                .filter((page) =>
                {
                    return tags.every((tag) => page.tags.includes(tag));
                });
        }

        return pages;
    }

    buildNestedPages(pages : WikiPageSummary[], path : string) : NestedPages[]
    {
        if(path.startsWith('/'))
        {
            path = path.substring(1);
        }

        const nestedData = pages.reduce((data, page) =>
        {
            const pathParts = page.path
                .split('/')
                .filter((part) => part.length > 0);

            let newPath = '';
            let pageArray = data;
            pathParts.forEach((pathPart, index) =>
            {
                if(newPath === '')
                {
                    newPath = pathPart;
                }
                else
                {
                    newPath += `/${ pathPart }`;
                }

                let newPage = pageArray.find((item) => item.path === newPath);

                if(!newPage)
                {
                    newPage = {
                        path: newPath
                    };

                    pageArray.push(newPage);
                }

                // If we're not the last item...
                if(index < (pathParts.length - 1))
                {
                    newPage.children = newPage.children ?? [];
                    pageArray = newPage.children;
                }
                else
                {
                    newPage.title = page.title;
                }
            });

            return data;
        }, [] as NestedPages[]);

        // Now that we have the data, we need to traverse the path, and only return the correct sub object
        const pathParts = path.split('/')
            .filter((part) => part.length > 0);

        let newPath = '';
        return pathParts.reduce((nested, part) =>
        {
            if(newPath === '')
            {
                newPath = part;
            }
            else
            {
                newPath += `/${ part }`;
            }

            const page = nested.find((item) => item.path === newPath);
            if(page)
            {
                return page.path === path ? [ page ] : page.children ?? [];
            }
            else
            {
                return nested;
            }
        }, nestedData);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new PageEngine();

//----------------------------------------------------------------------------------------------------------------------
