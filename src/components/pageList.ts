// ---------------------------------------------------------------------------------------------------------------------
// Page List Component
// ---------------------------------------------------------------------------------------------------------------------

// Managers
import pageMan from '../lib/managers/page.ts';

// Models
import { NestedPages } from '../models/pageList.ts';

// ---------------------------------------------------------------------------------------------------------------------

export class PageList extends HTMLElement
{
    #initialized = false;
    #root ?: HTMLElement;

    constructor()
    {
        super();
    }

    #buildLink(page : { title : string, path : string }) : HTMLAnchorElement
    {
        const aElem = document.createElement('a');
        aElem.setAttribute('href', `/${ page.path }`);
        aElem.className = 'is-internal-link is-valid-page';
        aElem.innerText = `${ page.title }`;

        return aElem;
    }

    #buildElems(pages : NestedPages[]) : HTMLUListElement
    {
        const ulElem = document.createElement('ul');
        pages.forEach((page) =>
        {
            const liElem = document.createElement('li');
            if(!page.title)
            {
                liElem.innerText = `/${ page.path }`;
            }
            else
            {
                const aElem = this.#buildLink({ title: page.title, path: page.path });
                liElem.appendChild(aElem);
            }

            if(page.children && page.children.length > 0)
            {
                const innerElem = this.#buildElems(page.children);
                liElem.appendChild(innerElem);
            }

            ulElem.appendChild(liElem);
        });

        return ulElem;
    }

    connectedCallback() : void
    {
        if(!this.#initialized)
        {
            this.#initialized = true;

            // Get attributes
            const path = this.getAttribute('path') ?? '/';
            const filter = this.getAttribute('filter') ?? undefined;
            const tags = (this.getAttribute('tags') ?? '')
                .split(',')
                .map((item) => item.trim())
                .filter((item) => !!item);

            // Get Pages
            pageMan.getPages(path, filter, tags)
                .then(async (pages) =>
                {
                    // Build inner elements
                    this.#root = this.#buildElems(pages);

                    // Prepare our element
                    this.#root.className = 'page-list';

                    this.append(this.#root);
                });
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
