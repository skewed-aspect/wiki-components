//----------------------------------------------------------------------------------------------------------------------
// Vite Config
//----------------------------------------------------------------------------------------------------------------------

import path from 'node:path';
import { defineConfig } from 'vite';
import { IncomingMessage, ServerResponse } from 'http';

//----------------------------------------------------------------------------------------------------------------------

type NextFunction = (err ?: any) => void;

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const fauxServer = () => ({
    name: 'wikijs-faux-server',
    configureServer(server)
    {
        server.middlewares.use((req : IncomingMessage, res : ServerResponse, next : NextFunction) =>
        {
            if(req.method.toLowerCase() === 'post')
            {
                if(req.url === '/graphql')
                {
                    res.setHeader('Content-Type', 'application/json');
                    res.end(JSON.stringify({
                        data: {
                            pages: {
                                list: [
                                    { id: 1, path: 'test/page1', title: 'Test Page 1', tags: [ 'test' ] },
                                    { id: 3, path: 'test/page2', title: 'Test Page 2', tags: [ 'test', 'foobar' ] },
                                    { id: 31, path: 'test/page2/extra', title: 'Nested Extra Page', tags: [ 'test', 'extra', 'long name' ] },
                                    { id: 35, path: 'test/page3', title: 'Test Page 3', tags: [ 'long name' ] },
                                    { id: 35, path: 'test/nested1/nested2/nested-page', title: 'Very Nested Page', tags: [ 'extra', 'long name' ] },
                                    { id: 36, path: 'bar', title: 'Test Page Bar', tags: [ 'bar' ] }
                                ]
                            }
                        }
                    }));

                    return;
                }
            }

            next();
        });
    }
});

//----------------------------------------------------------------------------------------------------------------------

export default defineConfig({
    root: 'src/demo',
    publicDir: 'assets',
    plugins: [ fauxServer() ],
    server: {
        port: 4200
    },
    build: {
        lib: {
            entry: path.resolve(__dirname, 'src/components.ts'),
            name: 'WikiJSWebComponents',
            fileName: (format) => `wikijs-components.${ format }.js`
        },
        outDir: '../../dist/',
        emptyOutDir: true,
        cssCodeSplit: true
    }
});

//----------------------------------------------------------------------------------------------------------------------
